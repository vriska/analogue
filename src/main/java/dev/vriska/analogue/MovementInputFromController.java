package dev.vriska.analogue;

import net.minecraft.util.MovementInput;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controllers;

public class MovementInputFromController extends MovementInput {
    private final MovementInput inner;

    private float xAxisValue = 0.0f;
    private float yAxisValue = 0.0f;
    private int lastXController = -1;
    private int lastYController = -1;
    private boolean creationFailed = false;

    public MovementInputFromController(MovementInput inner) {
        this.inner = inner;
    }

    private void pollControllers() {
        if (creationFailed) return;

        try {
            Controllers.create();
        } catch (LWJGLException e) {
            creationFailed = true;
            Analogue.LOGGER.warn("Failed to initialize controllers", e);
            return;
        }

        Controllers.poll();

        while (Controllers.next()) {
            if (Controllers.isEventXAxis()) {
                if (this.lastXController != -1 && Controllers.getEventControlIndex() != this.lastXController) {
                    if (Math.abs(this.xAxisValue) > Math.abs(Controllers.getEventXAxisValue())) {
                        continue;
                    }
                }

                this.xAxisValue = Controllers.getEventXAxisValue();
                this.lastXController = Controllers.getEventControlIndex();
            }

            if (Controllers.isEventYAxis()) {
                if (this.lastYController != -1 && Controllers.getEventControlIndex() != this.lastYController) {
                    if (Math.abs(this.yAxisValue) > Math.abs(Controllers.getEventYAxisValue())) {
                        continue;
                    }
                }

                this.yAxisValue = Controllers.getEventYAxisValue();
                this.lastYController = Controllers.getEventControlIndex();
            }
        }
    }

    @Override
    public void updatePlayerMoveState() {
        this.inner.updatePlayerMoveState();

        this.pollControllers();

        this.moveStrafe = this.inner.moveStrafe;
        this.moveForward = this.inner.moveForward;
        this.jump = this.inner.jump;
        this.sneak = this.inner.sneak;

        this.moveStrafe -= this.xAxisValue;
        this.moveForward -= this.yAxisValue;

        if (this.moveStrafe < -1.0f) this.moveStrafe = -1.0f;
        if (this.moveStrafe > 1.0f) this.moveStrafe = 1.0f;
        if (this.moveForward < -1.0f) this.moveForward = -1.0f;
        if (this.moveForward > 1.0f) this.moveForward = 1.0f;
    }
}
