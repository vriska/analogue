package dev.vriska.analogue;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = "analogue", name = "Analogue", version = "0.1")
public class Analogue {
    public static final Logger LOGGER = LogManager.getLogger("analogue");

    public Analogue() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    @SideOnly(Side.CLIENT)
    public void livingUpdate(LivingEvent.LivingUpdateEvent event) {
        if (event.entity instanceof EntityPlayerSP player) {
            if (player.movementInput != null && !(player.movementInput instanceof MovementInputFromController)) {
                player.movementInput = new MovementInputFromController(player.movementInput);
            }
        }
    }
}
